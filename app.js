// alert("Hello, Batch 176!");

// While Loop
/*

    A while loop takes in expression/condition

    Syntax:

        while(expression/condition) {
            statement
        }

*/

// let count = 5;

// while (count != 0) {
//     console.log("While: " + count);
//     count--
// }

// while (count > 0) {
//     console.log(count)
//     count -= 1
// }

/*
        Mini Activity:
        Should only display the numbers 1-5
        Correct the following loop.

            let x = 0

            while(x<5){
                console.log(x)
                x+=1
            }


*/


//         Mini Activity Solution:
// let x = 1;

// while (x <= 5) {
//     console.log(x)
//     x++ 1
// }


// Do While Loop
/*
            Do While Loops guarantee that the code will be executed at least once.

            Syntax:
            do {
                statement
            } while (condition/expression)
*/

// let number = Number(prompt("Give me a number:"));

// do {
//     console.log("Do While: " + number);
//     number += 1
// } while (number <= 10);


// For Loop
/*
    Syntax:
            for (initialization; expression/condition; finalExpression ){
                statement
            }
*/

// for (let count = 0; count <= 20; count++) {
//     console.log("For Loop: " + count);
// }

// let myString = "alex";

// // console.log(myString.length);
// // console.log(myString[0]);
// // console.log(myString[3]);
// // console.log(myString[1]);

// for (let x = 0; x < myString.length; x++) {
//     console.log(myString[x])
// }

// let myName = "Jeremiah";

// for (let i = 0; i < myName.length; i++) {
//     if (
//         myName[i].toLowerCase() == "a" ||
//         myName[i].toLowerCase() == "e" ||
//         myName[i].toLowerCase() == "i" ||
//         myName[i].toLowerCase() == "o" ||
//         myName[i].toLowerCase() == "u"
//     ) {
//         console.log("vowel")
//     } else {
//         console.log(myName[i])
//     }
// }

// For Loop (Continue and Break Statement)
/*
    "break" statement is used to terminate the current loop once a match is found

    "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.

*/

// for (let count = 0; count <= 20; count++) {
//     console.log("Hello!")
//     // count = 0
//     if (count % 2 === 0) {
//         console.log("Even Number: " + count)
//         continue;
//     }
//     console.log("Continue and Break: " + count)
//     if (count > 10) {
//         break;
//     }
// }

let name = "majoha";

for (let i = 0; i < name.length; i++) {
    console.log(name[i])

    if (name[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration");
        continue;
    }

    if (name[i] === "h") {
        break;
    }
}